---
layout: page
title: github projects
permalink: github/index.html
---

Personal projects
-----------------

###Rich Text Excerpts

* [Main plugin page](rich-text-excerpts/)
* [Wordpress plugins page](http://wordpress.org/plugins/rich-text-excerpts/)
* [Github project page](https://github.com/bjorsq/rich-text-excerpts)

###jquery.inlinemultiselect

* [Main plugin page, with examples](inlinemultiselect/)
* [Github project page](https://github.com/bjorsq/inlinemultiselect)

p-2 projects
------------

###swish-e-press

* [Main plugin page](swish-e-press/)
* [Github project page](https://github.com/p-2/swish-e-press)

###p-2 wordpress theme

* [Main theme page](p2-theme/)
* [Github project page](https://github.com/p-2/p2-theme)
