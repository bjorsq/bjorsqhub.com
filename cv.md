---
layout: page
title: curriculum vitae
permalink: cv/index.html
---

Objectives
----------

I am currently working part-time (0.5) at the University of Leeds as the web developer for the [Faculty of Education, Social Sciences and Law](http://www.essl.leeds.ac.uk), and part time (0.5) as a developer within the [Faculty of Performance, Visual Arts and Communications](http://www.pvac.leeds.ac.uk/). I'm also the technical director of the company e-2.org limited, whose current activities largely consist of working on the websites [healthylunch.org.uk](http://healthylunch.org.uk) and [ianritchiearchitects.co.uk](http://www.ianritchiearchitects.co.uk).

I am always looking for a new position in which I am encouraged to learn new skills, and utilise my current abilities on creative and interesting projects. One particular area where I wish to develop my skills the application of Agile methodologies to problem solving and software development.

I am very hard working and committed to all the projects I undertake, and enjoy working with other individuals who have different skill sets. I love to solve seemingly intractable problems, both in programming and in those involving interpersonal dynamics between stakeholders in the projects I am involved with. My main strength is an ability to think outside the box and employ lateral approaches to problems. I thrive under pressure, and the most rewarding part of web development for me is applying new skills and new technologies to projects to add an extra dimension to them.

Skills
------

### Programming:

* PHP (15 years)
* Perl (11 years)
* ASP (3 years)
* JavaScript (17 years)
* XHTML/HTML (17 years)
* CSS (14 years)
* XML/SVG (12 years)
* Flash/ActionScript (5 years)

### Software:

* Extensive knowledge of Windows, Mac and Linux operating systems and software.
* Linux (RHEL, Fedora, Ubuntu, CentOS, Debian) system administration.
* Web server configuration and administration (Apache 2).
* Database configuration and administration (MySQL, Postgres).
* Mail server configuration and administration (Netscape Messaging Server, qmail, sendmail).
* Web authoring packages and IDEs including Sublime Text, eclipse and Visual InterDev.
* Image editing and multimedia packages including Photoshop, Illustrator, Gimp and Inkscape.

Experience
----------

I have a broad range of programming skills utilising several different web technologies, but I am committed to using open source software. My PHP skills are excellent and I use Obeject-oriented design (utilising PHP5) and design patterns in all of my projects. I am currently working with the [modx content management framework](http://modxcms.com), [Zend Framework](framework.zend.com) and [Wordpress](www.wordpress.org). I have extensive knowledge of XHTML, XML, SVG, JavaScript/ECMAScript, and in-depth knowledge of a number of JavaScript frameworks, including [Prototype](http://www.prototypejs.org), [jQuery](http://jquery.com) and [Backbone](http://backbonejs.org/). I also have extensive experience in the use of Google's web services and APIs (AJAX, Search and mapping). I have knowledge of the full development lifecycle from the work I did with e-2 on behalf of the Royal Academy of Arts, where I wrote detailed technical and functional specifications and developed my own systems on top of an existing codebase. I use subversion and git to manage my projects.

I am currently developing my skills in the use of PHP frameworks and the Wordpress API, and using jQuery extensively in the web applications I develop to increase their responsiveness and interactivity in an unobtrusive, accessible and standards-compliant way.

Interests
---------

My main personal interests are in art, music and literature. I have published a small run of cassettes containing sounds produced by manipulating samples using a computer and have also had a small piece of music featured on a 12" vinyl E.P.. I love to read, and my main fields of interest are 20th Century fiction, music and art. I also try to keep abreast of developments in contemporary art and visit galleries regularly.

Education
---------

I attended schools in Lancashire, where I learnt enough to get into Leicester University in 1988 to study Chemistry with Biochemistry. I left university with a BSc (Hons) 2(ii) degree in Chemistry. In 1995 I started a Postgraduate Diploma in Information Management at the University of North London while working in Newham's Library Service as a librarian. I paid for the course and elected to job share in order to free up time for my studies, as my employers refused to support me (I am highly self-motivated and committed to continuing professional development). Some aspects of my degree (computer modelling water molecules to investigate solvation) and the diploma (fortran programming, HTML and Javascript, and cataloguing) made me realise that my interests lay in programming and markup languages. I have spent the last 15 years undergoing a programme of study - I learn new concepts very quickly and easily and I continually strive to improve the quality of my work.

Employment History
------------------

I left University with a strong desire to go to sea and become a sailor, and worked on the ferries out of Dover for a year and a half to secure my seaman's papers. Unfortunately, the job market for container ships and tankers was quite depressed when I was trying to secure a job, so I took up a position on a cruise ship, which destroyed my desire for a seagoing life quite quickly. I love books and reading, and became a librarian - a job which I thoroughly enjoyed for 10 years until I realised that working in the public sector as a librarian was becoming a rather tenuous career choice. During a Postgraduate course in information management, I became fascinated by web technologies. A few years after completing the course, I embarked on a career as an intranet development officer on the strength of skills I had built up, and worked in this position for three years before starting out with e-2. I moved with my family to West Yorkshire in 2009 and took up a position at the University of Leeds.
