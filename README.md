bjorsq.net
==========

This is my personal website hosted on Github.

Takes feeds from Flickr, Vimeo and Dropbox for pictures, video and music.

Built using [jekyll](http://jekyllrb.com/) with [jekyll bootstrap](http://jekyllbootstrap.com/)